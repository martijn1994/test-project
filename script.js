// 'use strict';

const projects = [
  //[0] = number
  //[1] = title
  //[2] = story
  //[3] = images
  //[4] = image sizes
  //[5] = captions
  //[6] = videos
  //[7] = video height in %
  //[8] = tags

  //here starts project 1
  [
    ' FlightGA-974',
    'Flight GA-974 is a filmic reconstruction of the Munir Said Thalib homicide, based primarily upon interviews with case lawyer Martha Meijer and head of Amnesty Indonesia Usman Hamid. made in Collaboration with Auke Lansink & Samuel Rynearson for the expostion Models for Humanity. In celebration of hunderd year anniversary of the Dutch branch of Amnesty International',
    [
      'FlightGA-974_Amnesty_Auke_Martijn_Sam.mp4',
      80,
      // 'IMG_0244.jpg',
      // 60,
      // '',
      'IMG_1140.jpg',
      80,
      'photoscan.png',
      80,
      'amnesty_wheels.png',
      80,
    ],
    ['video', 'typography'],
  ],
  //here starts project 2
  [
    ' Drawbot Typeface ',
    'An applictation written in python using the drawbot extension. With the application you can write a word and with a custom interactive font made up of circles. by changing the in the parameters in the application the font changes appearance. ',
    ['pytontype.mp4', 80, 'hallo.png', 80, 'python2.png', 80],
    ['', 'typography'],
  ],

  //here starts project 3
  [
    ' light color study ',
    'Study on the working of color in light ',
    ['00085.mp4', 70, 'prism.jpg', 70, 'IMG_7357.jpg', 70, 'IMG_7359.jpg', 70],
    [''],
  ],

  //here starts project 4
  [
    ' Typebooster/Goku Typeface ',
    'Typebooster is a reader that helps new type designers to find the best program for digitizing typhography.',
    [
      'typebooster.jpg',
      60,
      'typebooster2.jpg',
      60,
      's.png',
      60,
      'manga2.png',
      60,
      'goku_s.png',
      60,
    ],
    ['typography'],
  ],

  //here starts project 5
  [
    ' IISG publication ',
    'publication showing the  ',
    [
      'iisg1.jpg',
      80,
      'iisg.jpg',
      80,
      'iisg2.jpg',
      80,
      'iisgamsterdam.jpg',
      80,
      'The IISG is a ',
    ],
    [''],
  ],
];

//adds the number of the project in each of the projects.
for (let i = 0; i < projects.length; i++) {
  projects[i].unshift(i + 1);
}

const pageEl = document.querySelector('#page');
const menuEl = document.querySelector('#menu');
const headerEl = document.querySelector('#header');
const pageElWidth = window.innerWidth - 300;

///////////////title div, function that makes div for project title
const createTitleDiv = function (project) {
  const div = document.createElement('div');
  menuEl.appendChild(div);
  div.textContent = project[1];
  div.classList.add('link');

  //creates a css style that is unique to the project number.
  const titleStyle = 'titleSt' + project[0];
  div.classList.add(titleStyle);
};

///////////////story div, function that makes div for project story and puts it previously made title div
const createStoryDiv = function (project) {
  const motherDiv = '.titleSt' + project[0];
  const titleEl = document.querySelector(motherDiv);

  const div = document.createElement('div');
  titleEl.appendChild(div);
  div.textContent = project[2];
  div.style.paddingLeft = '20px';
  div.style.paddingBottom = '14px';
  div.style.paddingTop = '14px';
  div.style.fontSize = '14px';
  div.style.textAlign = 'left';

  //creates a css style that is unique to the project number
  const storyStyle = 'storySt' + project[0];
  div.classList.add(storyStyle);
  div.classList.add('hidden');
};

///////////////image div, function that makes and filles div for project content
const createImgDiv = function (project) {
  const div = document.createElement('div');
  pageEl.appendChild(div);

  //creates a css style that is unique to the project number
  const imageStyle = 'imageSt' + project[0];
  div.classList.add(imageStyle);
  div.classList.add('hidden');

  //function that sets the styling of the images & videos.
  const setContentStyling = function (
    projectImg,
    projectImgWidth,
    projectImgKind
  ) {
    const imageWidth = projectImgWidth;
    // creates an element for either image or video.
    let element = document.createElement(projectImgKind);
    element.setAttribute('src', 'images/' + projectImg);
    element.style.width = imageWidth + '%';
    div.appendChild(element);
    if (projectImg.includes('.mp4') === true) {
      element.setAttribute('controls', 'controls');
    }
  };

  // checks if content is either an image or a video.
  for (let i = 0; i < project[3].length; i = i + 2) {
    if (
      project[3][i].includes('.png') === true ||
      project[3][i].includes('.jpg') === true
    ) {
      setContentStyling(project[3][i], project[3][i + 1], 'img');
    } else if (project[3][i].includes('.mp4')) {
      setContentStyling(project[3][i], project[3][i + 1], 'video');
    }
  }
};

///////////// function that runs all other functions and creates a div structure for project.
const genDivStruc = function (project) {
  createImgDiv(project);
  createTitleDiv(project);
  createStoryDiv(project);
};

for (let i = 0; i < projects.length; i++) {
  genDivStruc(projects[i]);
}

/////////////// create a clickfunction for every project
for (let i = 1; i < projects.length + 1; i++) {
  let titleEl = document.querySelector('.titleSt' + i);
  let storyEl = document.querySelector('.storySt' + i);
  let imageEl = document.querySelector('.imageSt' + i);

  titleEl.addEventListener('click', function () {
    if (imageEl.classList.contains('hidden') === true) {
      // hide all projects so previously opened projects will close
      for (let i = 1; i < projects.length + 1; i++) {
        let storyEl = document.querySelector('.storySt' + i);
        let imageEl = document.querySelector('.imageSt' + i);

        imageEl.classList.add('hidden');
        storyEl.classList.add('hidden');
      }
      // show show the project that is clicked
      imageEl.classList.remove('hidden');
      storyEl.classList.remove('hidden');
      pageEl.style.paddingBottom = '10px';
    } else {
      imageEl.classList.add('hidden');
      storyEl.classList.add('hidden');
      $('hidden').fadeOut('slow');
      // document.body.style.backgroundColor = 'white';
    }
  });
}
